---
layout: page
title: "Getting Started"
category: doc
date: 2015-10-19 11:49:25
disqus: 2015-10-19-getting-started
order: 1
---

In this tutorial, we're going to learn by examples how to install, import and use verto jQuery library to create a basic video conference web application.

## What will be covered

- [installing verto](installing-verto.html)
- [importing and initializing verto](initializing-verto.html)
- [making an audio call](making-a-call.html)
- [hanging up a call](hanging-up-a-call.html)
- [answerign a call](answering-a-call.html)
- [adding video support](adding-video-support.html)
- [recovering a call](recovering-a-call.html)
